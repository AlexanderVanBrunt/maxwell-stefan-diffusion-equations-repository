import math as m
import numpy as np
from firedrake import *
import matplotlib.pyplot as plt
import sys, time
Citations.print_at_exit()


#########################################################################
# 1. Define the physical parameters (set to 1 without loss of generality)
#########################################################################

n = 4  # Number of species.

# RT is set to one without any loss of generality
Rgas = 1  # Ideal gas constant
T = 1  # Temperature

# Now we define the matrix of diffusion coefficients
Diffcoefmatrix = [[1, 2, 1, 1], [2, 1, 1, 1], [1, 1, 1, 3], [1, 1, 3, 1]]

# Although the molar masses are all set to 1 in this idealised example, we
# include them for completeness
M = [1 for i in range(0, n)]


##########################################
# 2). Set up the mesh we will iterate over
##########################################

# Define the error matrix. We will use this to plot the error plot
Error = np.zeros((4, 4))

for N in [8, 16, 32, 64]:
    mesh = UnitSquareMesh(N, N)

    ###############################
    # 3. Define the function spaces
    ###############################

    # a). Function space for concentration (for each species)
    X = FunctionSpace(mesh, "CG", 2)

    # b). Function space for species velocity
    Q = VectorFunctionSpace(mesh, "DG", 1)

    # c) Procedure to define "Final function space"
    XX = X
    QQ = Q
    # Use a loop to define the function spaces over n
    for i in range(1, n):
        XX = XX * X
        QQ = QQ * Q

    W = XX * QQ  # "Final function space"

    U_n = Function(W)
    U = TrialFunction(W)
    UU = TestFunction(W)
    print("The number of degrees of freedom of the problem: ", end="")
    print(W.dim())
    # d). Unpack the function space to define the trial and test functions.
    # c_{i} is concentration of the ith species, v_{i} is velocity of ith
    # species
    (c1, c2, c3, c4, v1, v2, v3, v4) = split(U)
    (w1, w2, w3, w4, tau1, tau2, tau3, tau4) = split(UU)

    # Use the same notation as the paper for velocity and test spaces.
    tildec = [c1, c2, c3, c4]
    tildev = [v1, v2, v3, v4]
    tildew = [w1, w2, w3, w4]
    tildetau = [tau1, tau2, tau3, tau4]


    ########################################
    # 4. Construct the manufactured solution
    ########################################

    (x, y) = SpatialCoordinate(mesh)
    mass_flux = as_vector([0, 1])
    # a). Construct the real concentration
    K1 = Constant(1)
    K2 = K1
    k1 = (1 / 2) * exp(8 * (x * y * (1 - x) * (1 - y)))
    k2 = (1 / 2) * sin(pi * x) * sin(pi * y)
    realc1 = k1 + K1
    realc2 = -k1 + K1
    realc3 = k2 + K2
    realc4 = -k2 + K2
    # Can also set this as constant for simplifying. It should be 12.0 in
    # theory.
    realcT = realc1 + realc2 + realc3 + realc4
    realc = [realc1, realc2, realc3, realc4]  # Turn into list for convenience

    # b). Construct the velocity, useful to define these constants
    Constant1 = 2 * K1 * (1 / Diffcoefmatrix[0][1] + 1 / Diffcoefmatrix[0][3])
    Constant2 = 2 * K2 * (1 / Diffcoefmatrix[2][3] + 1 / Diffcoefmatrix[0][3])

    realv1 = -(realcT/(Rgas*T*Constant1))*grad(realc1)/realc1 + mass_flux/(realcT)
    realv2 = -(realcT/(Rgas*T*Constant1))*grad(realc2)/realc2 + mass_flux/(realcT)
    realv3 = -(realcT/(Rgas*T*Constant2))*grad(realc3)/realc3 + mass_flux/(realcT)
    realv4 = -(realcT/(Rgas*T*Constant2))*grad(realc4)/realc4 + mass_flux/(realcT)
    realv = [realv1, realv2, realv3, realv4]

    ###################################
    # 5. Set up the data to the problem
    ###################################

    # a). Define the reaction rates which must correspond to our solution
    r1 = div(realc1 * realv1)
    r2 = div(realc2 * realv2)
    r3 = div(realc3 * realv3)
    r4 = div(realc4 * realv4)

    Reactionratelist = [r1, r2, r3, r4]

    # b). We define the Dirichlet boundary conditions.
    bc1 = DirichletBC(W.sub(0), realc1, "on_boundary")
    bc2 = DirichletBC(W.sub(1), realc2, "on_boundary")
    bc3 = DirichletBC(W.sub(2), realc3, "on_boundary")
    bc4 = DirichletBC(W.sub(3), realc4, "on_boundary")
    bc = [bc1, bc2, bc3, bc4]

    ##############################
    # 6. Define the bilinear forms
    ##############################

    # a) Firstly we have to set up the initial guess. We take the initial guess
    # as 1 for all species
    prevc1 = Function(X).interpolate(Constant(1))
    prevc2 = Function(X).interpolate(Constant(1))
    prevc3 = Function(X).interpolate(Constant(1))
    prevc4 = Function(X).interpolate(Constant(1))
    prevc = [prevc1, prevc2, prevc3, prevc4]

    # We don't need this for the solver, but do need it to check the termination criterion
    prevv = [Function(Q).project(as_vector(Constant([0, 0]))) for i in range(0, n)]

    # This is the bilinear form associated to the steady state continuity
    # equation.
    bck = 0
    R = 0  # This is the linear functional which has the reaction rates
    for i in range(0, n):
        bck += (dot(grad(tildew[i]), prevc[i] * tildev[i])) * dx
        R += -Reactionratelist[i] * tildew[i] * dx

    gamma = 1  # The penalty parameter in the augmented Lagrangian approach.

    # Define the total concentration
    cT = Function(X).interpolate(prevc[0] + prevc[1] + prevc[2] + prevc[3])

    # Define the total sum of fluxes and total concentration.
    mass_flux_term = 0
    for i in range(0, n):
        mass_flux_term += prevc[i] * tildev[i] * M[i]

    # Define the main bilinear form. Note that the molar masses are all 1 in this test case,
    # so density is equal to the total concentration

    b = 0  # Bilinear form for concentration gradient
    A = 0  # Symmetric coercive bilinear form on Q*Q
    lck = 0  # Linear functional with the mass flux
    for i in range(0, n):
        b += Rgas * T * (dot(grad(tildec[i]), tildetau[i])) * dx
        A += gamma * (prevc[i] * M[i] / cT) * dot(mass_flux_term, tildetau[i]) * dx
        lck += gamma * (prevc[i] * M[i] / cT) * dot(mass_flux, tildetau[i]) * dx
    for i in range(0, n):
        for j in range(0, n):
            if (i != j):
                A += (Rgas * T * prevc[i] * prevc[j] / (Diffcoefmatrix[j][i] * cT)) * dot((tildev[i] - tildev[j]), tildetau[i]) * dx

    #####################################################
    # 7. Assemble the solver and specify solve parameters
    #####################################################

    # The summed bilinear forms
    Fsolv = A + b + bck
    L = lck + R

    # Specify the solver to solve directly using the mumps package
    sp = {"mat_type": "aij",
          "snes_monitor": None,
          "ksp_type": "preonly",
          "pc_type": "lu",
          "pc_factor_mat_solver_type": "mumps",
          }

    # We define the residual function which tells us the magnitude of the
    # change in the next members of the sequence.
    def Residual(U, prevc, prevv):
        red = 0
        for i in range(0, n):
            red += sqrt(assemble(dot(U[i] - prevc[i], U[i] - prevc[i]) * dx))
            red += sqrt(assemble(dot(grad(U[i]) - grad(prevc[i]), grad(U[i]) - grad(prevc[i])) * dx))
            red += sqrt(assemble(dot(U[i + n] - prevv[i], U[i + n] - prevv[i]) * dx))
        return red


    ###############################
    # 8. Solve the nonlinear system
    ###############################

    # Outer solve
    Runningred = 1  # Define the running residual
    counter = 1  # A counter of the number of non-linear iterations solves
    epsilon = 1e-13  # The tolerance level
    Totaltime = 0
    while (Runningred > epsilon):

        # Solve the linear system
        starttime = time.time()
        solve(Fsolv == L, U_n, bcs=bc, solver_parameters=sp)
        Solvetime = time.time()-starttime
        Totaltime += Solvetime
        U = split(U_n)

        # Calculate and print the residual
    
        Runningred = Residual(U, prevc, prevv)
        print("  Running residual: ", Runningred)

        # Update the total concentration
        cT.assign(Function(X).interpolate(U[0] + U[1] + U[2] + U[3]))

        # Check the Gibbs--Duhem equation
        print("  Error in Gibbs--Duhem equation on current iteration: ", end="")
        print(sqrt(assemble(dot(grad(cT), grad(cT)) * dx)))

        # Update the concentration for the next iteration
        for i in range(0, n):
            prevc[i].assign(Function(X).interpolate(U[i]))
            prevv[i].assign(Function(Q).project(U[i + n]))
        counter += 1

    ###################################################
    # 9. Analyse the Gibbs Duhem equation and the error
    #    of the mass flux of the converged solution
    ###################################################

    calculatedmass_flux = as_vector([0, 0])
    for i in range(0, n):
        calculatedmass_flux += M[i] * U[i] * U[i + n]

    print("Final error in Gibbs--Duhem relation: ", end="")
    print(sqrt(assemble(dot(grad(cT), grad(cT)) * dx)))
    print("Mass average velocity: ", end="")
    print(sqrt(assemble(dot(calculatedmass_flux - mass_flux,
                            calculatedmass_flux - mass_flux) * dx)))

    # Print the number of non-linear iterations required for convergence
    print("Number of non-linear iterations: ", counter)
    # Index the mesh size as a integer so we can store the error output
    k = round(m.log(N / 10, 2))
    avgtime = Totaltime/counter
    print("Average linear solve time:", avgtime)

    ##############################
    # 10. Error analysis and plots
    ##############################

    Error[k, 0] = 0
    Error[k, 1] = 0
    Error[k, 2] = 0

    # Define the three different types of error compared to the real solution.
    for i in range(0, n):
        Error[k, 0] += assemble(dot(U[i] - realc[i], U[i] - realc[i]) * dx)
        Error[k, 1] += assemble(dot(grad(U[i] - realc[i]), grad(U[i] - realc[i])) * dx)
        Error[k, 2] += assemble(dot(U[i + n] - realv[i], U[i + n] - realv[i]) * dx)
    Error[k, 0] = sqrt(Error[k, 0])
    Error[k, 1] = sqrt(Error[k, 1])
    Error[k, 2] = sqrt(Error[k, 2])
    Error[k, 3] = sqrt(assemble(
        dot(calculatedmass_flux - mass_flux, calculatedmass_flux - mass_flux) * dx))
    v1flux = Function(Q).project(U[4])  # Velocity field of species 1

    # Construct concentration and velocity plot for species 1
    c1 = Function(X).interpolate(U_n[0])
    plotsolution = False  # change to True to plot the solution
    if plotsolution:
        try:
            fig, axes = plt.subplots()
            colors = tripcolor(c1, axes=axes)
            fig.colorbar(colors)
        except Exception as e:
            warning("Cannot plot figure. Error msg: '%s'" % e)

        try:
            fig, axes = plt.subplots()
            colors = quiver(
                v1flux,
                axes=axes,
                headlength=5,
                headwidth=3,
                linewidths=0.2,
                minshaft=2)
            fig.colorbar(colors)
        except Exception as e:
            warning("Cannot plot figure. Error msg: '%s'" % e)

        plt.show()


#################################
# 11. Make the log-log error plot
#################################

# Calculate the log error
logError = np.log2(Error)  # Compute the log of each error term


# Plot the different error terms across the mesh size
plt.plot([1/8, 1/16, 1/32, 1/64], [1/2, 1/4, 1/8, 1/16], label="$O(h)$")
plt.plot([1/8, 1/16, 1/32, 1/64], [1/32, 1/128, 1/512, 1/2028], label="$O(h^{2})$")
plt.plot([1/8, 1/16, 1/32, 1/64], [1/512, 1/4056, 1/32448, 1/259584], label="$O(h^{3})$", color = 'black')
plt.plot([1/8, 1/16, 1/32, 1/64], Error[:,0], label="E1", linestyle='dashed', marker='*')
plt.plot([1/8, 1/16, 1/32, 1/64], Error[:,1], label="E2", linestyle='dashed', marker='D')
plt.plot([1/8, 1/16, 1/32, 1/64], Error[:,2], label="E3", linestyle='dashed', marker='o')
plt.plot([1/8, 1/16, 1/32, 1/64], Error[:,3], label="E4", linestyle='dashed', marker='x')
plt.xscale('log', basex=2)
plt.yscale('log', basey=2)
plt.xlabel('h')
plt.ylabel('Error')
plt.legend(loc='lower right')

plt.show()
