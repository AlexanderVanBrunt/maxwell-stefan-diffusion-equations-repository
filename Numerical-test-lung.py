from firedrake import *

###################################
# 1. Define the physical parameters
###################################

n = 4  # Number of species.

Rgas = 8.314  # Ideal gas constant
T = 298  # Temperature

# We will enumerate the species as follows.
# 1 is nitrogen
# 2 is oxygen
# 3 is carbon dioxide
# 4 is water vapour

# Now we define the matrix of diffusion coefficients
Diffcoefmatrix = [[1, 21.87, 16.63, 23.15],
                  [21.87, 1, 16.4, 22.85],
                  [16.63, 16.4, 1, 16.02],
                  [23.15, 22.85, 16.02, 1]]

# Define the molar mass of each species
M = [28, 32, 44, 18]

# Our mass flux, u
mass_flux = Constant((0, 0, 0))

###############################
# 2. Define the function spaces
###############################
# a). Import mesh

# Import the lung mesh file

mesh = Mesh("3Dlung.msh")

# Function space for concentration
X = FunctionSpace(mesh, "CG", 1)

# b). Function space for species velocity
Q = VectorFunctionSpace(mesh, "DG", 0)

# c) Procedure to define "Final function space"
XX = X

QQ = Q
# For loop to define the function spaces over n
for i in range(1, n):
    XX = XX*X
    QQ = QQ*Q

W = XX*QQ  # "Final function space"

# Only print out on rank 0
stdprint = print
def print(x, *args, **kwargs):
    if mesh.comm.rank == 0:
        stdprint(x, *args, **kwargs, flush=True)


print("The number of degrees of freedom of the problem: ", end="")
print(W.dim())
U_n = Function(W)
U = TrialFunction(W)
UU = TestFunction(W)

# d). Unpack the function space to define the trial and test functions.
# c_{i} is concentration of the ith species, v_{i} is velocity of ith species

(c1, c2, c3, c4, v1, v2, v3, v4) = split(U)
(w1, w2, w3, w4, tau1, tau2, tau3, tau4) = split(UU)

# Use the same notation as the paper for velocity and test spaces.
tildec = [c1, c2, c3, c4]
tildev = [v1, v2, v3, v4]
tildew = [w1, w2, w3, w4]
tildetau = [tau1, tau2, tau3, tau4]
# Finally compute the volume of the mesh, we will use this later.
Vol = assemble(Constant(1)*dx(mesh))

##########################
# 3. Set the boundary data
##########################


# These are the mole fraction profiles of alveolar and humid air
Alveolar_air = [Constant(0.7490), Constant(0.1360), Constant(0.0530), Constant(0.0620)]
Humid_air = [Constant(0.7409), Constant(0.1967), Constant(0.0004), Constant(0.0620)]


# We define the boundary conditions. The marker 3 denotes the 'top' and 2 the 'bottom'.
bctopN = DirichletBC(W.sub(0), Alveolar_air[0], 3)
bctopO2 = DirichletBC(W.sub(1), Alveolar_air[1], 3)
bctopC02 = DirichletBC(W.sub(2), Alveolar_air[2], 3)
bctopH20 = DirichletBC(W.sub(3), Alveolar_air[3], 3)
bcbottomN = DirichletBC(W.sub(0), Humid_air[0], 2)
bcbottomO2 = DirichletBC(W.sub(1), Humid_air[1], 2)
bcbottomC02 = DirichletBC(W.sub(2), Humid_air[2], 2)
bcbottomH20 = DirichletBC(W.sub(3), Humid_air[3], 2)

# roll up into one list
bc = [bctopN, bctopO2, bctopC02, bctopH20,
      bcbottomN, bcbottomO2, bcbottomC02, bcbottomH20]

# There is no reaction rate
Reactionratelist = [Constant(0) for i in range(0, n)]

##############################
# 4. Define the bilinear forms
##############################

# a) Firstly we have to set up the initial guess. We take the initial guess as
prevc = [Function(X).interpolate(Alveolar_air[i]) for i in range(0, n)]

# Our initial guess is Alveolar air. Firedrake automatically adjusts this
# initial guess to be compliant with the boundary conditions. We don't need
# this for the solver, but do need it to check the termination criterion
prevv = [Function(Q) for i in range(0, n)]


# This is the bilinear form associated to the steady state continuity equation.
bck = 0
R = 0  # This is the linear functional which has the reaction rates
for i in range(0, n):
    bck += (dot(grad(tildew[i]), prevc[i] * tildev[i])) * dx
    R += Reactionratelist[i] * tildew[i] * dx


gamma = 0.0004  # The penalty parameter in the augmented Lagrangian approach.

# Define the total concentration and density
cT = Function(X).interpolate(prevc[0]+prevc[1]+prevc[2]+prevc[3])
density = Function(X).interpolate(prevc[0]*M[0]+prevc[1]*M[1]+prevc[2]*M[2]+prevc[3]*M[3])

# Define the total sum of fluxes and total concentration.
mass_flux_term = 0
for i in range(0, n):
    mass_flux_term += prevc[i]*tildev[i]*M[i]

# Define the main bilinear form.

b = 0  # Bilinear form for concentration gradient
A = 0  # Symmetric coercive bilinear form on Q*Q
lck = 0  # Linear functional with the mass flux
for i in range(0, n):
    b += Rgas * T * (dot(grad(tildec[i]), tildetau[i])) * dx
    A += Rgas * T * gamma * (prevc[i] * M[i] / density) * dot(mass_flux_term, tildetau[i]) * dx
    lck += Rgas * T * gamma * (prevc[i] * M[i] / density) * dot(mass_flux, tildetau[i]) * dx
for i in range(0, n):
    for j in range(0, n):
        if (i != j):
            A += (Rgas * T * prevc[i] * prevc[j]/(Diffcoefmatrix[j][i] * cT))*dot((tildev[i] - tildev[j]), tildetau[i]) * dx

############################################
# 5. Assemble the solver and specify options
############################################

# The summed bilinear forms
Fsolv = A + b + bck
L = lck + R


# Specify the solver to solve directly using the mumps package
sp = {"mat_type": "aij",
      "snes_monitor": None,
      "ksp_type": "preonly",
      "pc_type": "lu",
      "pc_factor_mat_solver_type": "mumps",
      }

# We define the residual function to be the magnitude of the update.


def Residual(U, prevc, prevv):
    red = 0
    for i in range(0, n):
        red += sqrt(assemble(dot(U[i] - prevc[i], U[i] - prevc[i])*dx))
        red += sqrt(assemble(dot(grad(U[i]) - grad(prevc[i]), grad(U[i]) - grad(prevc[i]))*dx))
        red += sqrt(assemble(dot(U[i+n] - prevv[i], U[i+n] - prevv[i])*dx))
    return red


###############################
# 6. Solve the nonlinear system
###############################

# Outer solve
Runningred = 1  # Define the running residual
counter = 1  # A counter of the number of linear solves
TOL = 1e-11  # The tolerance level
while (Runningred > TOL):

    # Solve the linear system
    solve(Fsolv == L, U_n, bcs=bc, solver_parameters=sp)
    U = split(U_n)

    # Calculate and print the residual
    Runningred = Residual(U, prevc, prevv)
    print("  Running residual: ", end="")
    print(Runningred)

    # Update the total concentration
    cT.assign(Function(X).interpolate(U[0]+U[1]+U[2]+U[3]))
    density.assign(Function(X).interpolate(U[0]*M[0]+U[1]*M[1]+U[2]*M[2]+U[3]*M[3]))
    print("  Mass flux error: ", end="")
    print(sqrt(assemble(dot(U[0]*U[0+n]*M[0]+U[1]*U[1+n]*M[1]+U[2]*U[2+n]*M[2]+U[3]*U[3+n]*M[3] -
                            mass_flux, U[0]*U[0+n]*M[0]+U[1]*U[1+n]*M[1]+U[2]*U[2+n]*M[2]+U[3]*U[3+n]*M[3]-mass_flux)*dx)))

    print("  Error in Gibbs--Duhem equation on current iteration: ", end="")
    # Check the Gibbs--Duhem equation
    print(sqrt(assemble(dot(grad(cT), grad(cT))*dx)))

    # Update the concentration for the next iteration
    for i in range(0, n):
        prevc[i].assign(Function(X).interpolate(U[i]))
        prevv[i].assign(Function(Q).interpolate(U[i+n]))
    counter += 1

    # Compute the Gibbs--Duhem equation and the mass average velocity of the converged solution
    calculatedmass_flux = as_vector([0, 0, 0])
    for i in range(0, n):
        calculatedmass_flux += M[i]*U[i]*U[i+n]

# The volume of the mesh is approximately 4x10^{4} (mm^{2}), hence when computing the errors it makes sense to scale by this amount.
print("Final error in Gibbs--Duhem relation (scaled by volume of the mesh): ", end="")
print((1/Vol)*sqrt(assemble(dot(grad(cT), grad(cT))*dx)))
print("Mass average velocity (scaled by the volume of the mesh): ", end="")
print((1/Vol)*sqrt(assemble(dot(calculatedmass_flux - mass_flux, calculatedmass_flux-mass_flux)*dx)))

#######################
# 7. Analyse the output
#######################

# Print the number of non-linear iterations required for convergence
print("Number of non-linear iterations: ", end="")
print(counter)

# b). Export data for analysis
Lungexperimentnitrogen = File("LungexperimentNitrogen.pvd")
Lungexperimentnitrogenflux = File("LungexperimentNitrogenflux.pvd")
Lungexperimentoxygen = File("LungexperimentOxygen.pvd")
Lungexperimentoxygenflux = File("LungexperimentOxygenflux.pvd")
Lungexperimentcarbondioxide = File("LungexperimentCarbondioxide.pvd")
Lungexperimentcarbondioxideflux = File("LungexperimentCarbondioxideflux.pvd")
Lungexperimentwater = File("LungexperimentWater.pvd")
Lungexperimentwaterflux = File("Lungexperimentwaterflux.pvd")
Lungexperimentnitrogen.write(Function(X).interpolate(U[0]))
Lungexperimentnitrogenflux.write(Function(Q).interpolate(U[n]))
Lungexperimentoxygen.write(Function(X).interpolate(U[1]))
Lungexperimentoxygenflux.write(Function(Q).interpolate(U[1+n]))
Lungexperimentcarbondioxide.write(Function(X).interpolate(U[2]))
Lungexperimentcarbondioxideflux.write(Function(Q).interpolate(U[2+n]))
Lungexperimentwater.write(Function(X).interpolate(U[3]))
Lungexperimentwaterflux.write(Function(Q).interpolate(U[3+n]))
