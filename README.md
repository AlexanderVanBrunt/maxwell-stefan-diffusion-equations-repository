This is the repository containing the exact code used the numerical experiments in the paper "Augmented saddle point formulation of the steady-state Maxwell--Stefan diffusion equations".
It also contains the lung mesh used for the second numerical experiment.

The exact software used to run these scripts can be installed from Zenodo.
